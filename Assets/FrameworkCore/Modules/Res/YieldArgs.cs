﻿/*
 *  date: 2018-05-29
 *  author: John-chen
 *  cn: 异步加载的参数
 *  en: todo:
 */

using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// 异步加载的参数
    /// </summary>
    public class YieldArgs
    {
        /// <summary>
        /// 传入参数
        /// </summary>
        public object[] Args { get { return _args;} }

        /// <summary>
        /// 资源的路径
        /// </summary>
        public string Path { get { return _path;} }

        /// <summary>
        /// 是否加载完成
        /// </summary>
        public bool IsDone { get { return _isDone;} }

        /// <summary>
        /// 加载进度
        /// </summary>
        public float Progress { get { return _progress;} }

        /// <summary>
        /// 加载完成的对象
        /// </summary>
        public object Obj { get { return _obj;} }

        /// <summary>
        /// 加载完成后的对象
        /// </summary>
        public GameObject gameObject
        {
            get { return _gameObject;}
            set { _gameObject = value; }
        }


        public YieldArgs(params object[] args)
        {
            _args = args;
            _path = (string)args[0];
        }

        /// <summary>
        /// 加载完成的设置
        /// </summary>
        /// <param name="obj"></param>
        public void LoadDone(object obj)
        {
            _isDone = true;
            _progress = 1;
            _obj = obj;
        }

        /// <summary>
        /// 初始化自己
        /// </summary>
        /// <param name="args"></param>
        public void InitSelf(params object[] args)
        {
            _args = args;
            _path = (string)args[0];
        }

        /// <summary>
        /// 清理自己
        /// </summary>
        public void CleanSelf()
        {
            _path = "";
            _isDone = false;
            _progress = 0;
            _obj = null;
            _args = null;
        }

        protected object _obj;
        protected string _path;
        protected bool _isDone;
        protected object[] _args;
        protected float _progress;
        protected GameObject _gameObject;
    }

}
