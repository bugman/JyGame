﻿/*
 *  date: 2018-05-29
 *  author: John-chen
 *  cn: ab资源引用计数
 *  en: todo:
 */


using UnityEngine;

namespace JyFramework
{
    /// <summary>
    /// 引用计数
    /// </summary>
    public class RefAssetBundle
    {
        public RefAssetBundle()
        {
        }

        public RefAssetBundle(string name, AssetBundle ab):this()
        {
            _abRes = ab;
            _resName = name;
        }

        /// <summary>
        /// 增加引用
        /// </summary>
        public void AddRef()
        {
            _refCount += 1;
        }

        /// <summary>
        /// 减少引用
        /// </summary>
        public void DelRef()
        {
            _refCount -= 1;
            if(_refCount <= 0)
            {
                Destroy();
            }
        }        

        /// <summary>
        /// 清理自身
        /// </summary>
        public void ClearSelf()
        {
            Destroy();
        }

        /// <summary>
        /// 删除本引用的对象
        /// </summary>
        private void Destroy()
        {
            _abRes.Unload(true);
            MessageCtrl.Ins.NotifyEvent(ResEvent.DestoryRefAssetBundle, _resName);
        }

        private int _refCount;
        private string _resName;
        private AssetBundle _abRes;
    }
}
