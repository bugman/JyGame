﻿
namespace JyEditor
{

    public enum JyFileCombination
    {
        prefab,
        controller,
        fbx,
        unity,
        anim,
    };

    public enum JyFileUnit
    {
        png,
        mat,
        shader,
        ogg,
        custom,
        tga,
        txt,
    };


    public class FileFormatConfig : Singleton<FileFormatConfig>
    {

    }
}