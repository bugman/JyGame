﻿using GameCore.BaseCore;
using JyFramework;
using UnityEngine;

namespace GameCore.Test
{
    public class TestInput : MonoHelper
    {
        private void Awake()
        {

        }

        private void Start()
        {

        }

        private void OnEnable()
        {

        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                UIMgr.Ins.Show<TestWnd>();
            }

            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                UIMgr.Ins.Show<TestWnd2>();
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                UIMgr.Ins.Hide<TestWnd>();
            }

            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                UIMgr.Ins.Hide<TestWnd2>();
            }

            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                gameObject.AddComponent<TestNet>();
            }
        }
    }
}