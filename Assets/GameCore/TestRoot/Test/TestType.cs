﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestType : MonoBehaviour
{   

    void Start()
    {
        var tc = new TestClass();
        Set(tc);

        Test(tc, 1, 2, "xx");
        Test(null, 2, 3, "xxx");
    }


    void Update()
    {

    }

    void Set(object obj)
    {
        Type type = obj.GetType();

        Debug.Log(type.ToString());
    }

    void Test(TestClass tc = null, params object[] args)
    {
        if (tc != null)
        {
            Debug.Log(tc.name);
        }

        foreach (var arg in args)
        {
            Debug.Log(arg);
        }
    }
}


public class TestClass
{
    public string name = "TestClass";
}